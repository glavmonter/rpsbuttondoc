\chapter{USB}
Кнопка является классом USB - HID с пустым дескриптором (то есть не мышь, клавиатура и тд). Дополнительные драйвера для работы устройства не требуются.
Для работы с Кнопкой через \href{https://libusb.info/}{libusb} в Windows нужно будет поставить драйвер - враппер при помощи программы \href{https://zadig.akeo.ie/}{Zadig}.


\section{Что такое USB}
USB для обывателя - это подключение точка-точка. Точка называется Endpoint. Есть четыре типа Endpoint и с ними четыре типа передачи: 
\begin{itemize}
	\item Control Transfers;
	\item Interrupt Transfers;
	\item Bulk Transfers;
	\item Isochronous Transfers.
\end{itemize}

Подробно про USB можно почитать вот \href{https://www.beyondlogic.org/usbnutshell/usb1.shtml}{здесь}.

\subsection{Control Transfers}
Этот тип передачи обычно используется для передачи команд и статусов. После подключения устройства его обнаружение и настройка происходит посредством Control Transfers. Кнопка использует этот тип не явно и нам нет необходимости его обслуживать.

\subsection{Interrupt Transfers}
Основные параметры передачи:
\begin{itemize}
	\item Гарантированное время передачи;
	\item Двунаправленность;
	\item Определение ошибок и повторная отправка.
\end{itemize}

Максимальный размер пакета данных 8 байт для low-speed, 64 байта для full-speed и 1024 байта для high-speed устройств.

\subsection{Bulk Transfers}
Основные параметры:
\begin{itemize}
	\item Большие куски данных;
	\item Определение ошибок и гарантированная доставка;
	\item Не гарантируется широкая полоса и минимальная задержка;
	\item Устройство только Full-speed и High-speed. 
\end{itemize}

Размеры Endpoint для Full-speed устройств: 8, 16, 32 или 64 байта. Если размер данных меньше размера Endpoint, то нет необходимости дополнять его до требуемого размера. Передача считается завершённой, когда было передано точное количество данных, передан пакет меньше размера конечной точки или передан пакет нулевой длины.

\subsection{Isochronous Transfers}
Основные параметры:
\begin{itemize}
	\item Гарантированная широкая полоса передачи;
	\item Гарантированная задержка;
	\item Определение ошибок без повторной передачи.
\end{itemize}

Используется для передачи звука, видео и данных, где потеря части данных не важна (не наш случай).

\section{Параметры USB Кнопки}
Скорость интерфейса Full-speed (48 МБит/с). Три Endpoint:

\begin{tabular}{|c|c|c|c|c|}
	\hline 
	Адрес & Тип & Направление & Размер, байт & Интервал опроса, мс \\ 
	\hline 
	0x81 & Interrupt & IN & 64 & 5 \\ 
	\hline 
	0x82 & Bulk & IN & 64 & ----- \\ 
	\hline 
	0x02 & Bulk & OUT & 64 & ----- \\ 
	\hline 
\end{tabular} 
\\

Направление IN - это от устройства в хост (ПК), OUT - от ПК в устройство.
Интервал опроса для точки Bulk игнорируется.

Пакет данных имеет следующий вид (байты):
\\

\begin{bytefield}[bitwidth=5.0em]{64}
	\bitheader{0-4} \\
	\bitbox{1}{CMD} & \bitbox{1}{SIZE} & 
	\bitbox{1}{DATA\_0} & \bitbox{1}{DATA\_1} & \bitbox{1}{DATA...},
\end{bytefield}
где CMD - номер команды, SIZE - количество байт данных (0-62), DATA - собственно данные (максимум 62 байта).
Для сериализации данных использую \href{https://developers.google.com/protocol-buffers/}{Google Protocol Buffers} (версия синтаксиса 3).


Interrupt Transfer используется для передачи состояния кнопки: прикосновение, состояние Mifare, ParkPass и немного диагностической информации.

Критически важные данные передаются только через BULK Transfer.
Общая логика работы с BULK следующая:
\begin{enumerate}
	\item Запись Protobuf сообщения в 0x02 (OUT).
	\item Чтение ответа из 0x80 (IN).
	\item goto 1.
\end{enumerate}
